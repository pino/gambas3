#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: qt 3.8.90\n"
"POT-Creation-Date: 2020-11-18 18:53 UTC\n"
"PO-Revision-Date: 2015-09-18 02:27 UTC\n"
"Last-Translator: Gianluigi Gradaschi <bagonergi@gmail.com>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "QT application"
msgstr "Applicazione QT"

#: .project:2
msgid "A graphical application using the QT 5 component, or the QT 4 component if QT 5 is not available on the system."
msgstr "Un'applicazione grafica che utilizza il componente QT 5, o il componente QT 4 se QT 5 non è disponibile sul sistema."
