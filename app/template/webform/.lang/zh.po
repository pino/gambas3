#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: webform 3.15.90\n"
"POT-Creation-Date: 2020-11-18 18:53 UTC\n"
"PO-Revision-Date: 2020-11-18 18:53 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Web form application"
msgstr "Web窗体应用程序"

#: .project:2
msgid "A CGI script displaying a web form, and using the HTTP server component for debugging."
msgstr "CGI脚本显示在Web窗体。用HTTP服务器组件进行调试。"

#: Webform1.webform:16
msgid "<h2>Hello world!</h2>"
msgstr "<h2>世界，你好！</h2>"

#: Webform1.webform:26
msgid "Click me"
msgstr "点击这里"
