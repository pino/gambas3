' Gambas class file

Export

Public Enum TYPE_COLOR, TYPE_IMAGE, TYPE_LINEAR_GRADIENT, TYPE_RADIAL_GRADIENT

Public Type As Integer
Public X1 As Float
Public Y1 As Float
Public X2 As Float
Public Y2 As Float
Public Radius As Float
'Public Pattern As Image
Public (Color) As Integer
Public Colors As Integer[] = [0, &HFFFFFF&]
Public Positions As Float[] = [0, 1]
Public Path As String

' Private Sub RemoveOpacity(aColor As Integer[]) As Integer[]
' 
'   Dim I As Integer
'   
'   aColor = aColor.Copy()
'   For I = 0 To aColor.Max
'     aColor[I] = aColor[I] And &HFFFFFF&
'   Next
'   
'   Return aColor
' 
' End

Public Sub Copy() As CReportBrush
  
  Dim hBrush As New CReportBrush
  
  With hBrush
    .Type = Type
    .X1 = X1
    .X2 = X2
    .Y1 = Y1
    .Y2 = Y2
    .Radius = Radius
    .Color = Color
    .Colors = Colors.Copy()
    .Positions = Positions.Copy()
    .Path = Path
  End With
  
  Return hBrush
  
End


Public Function GetBrush(iX1 As Integer, iY1 As Integer, iX2 As Integer, iY2 As Integer) As PaintBrush
  
  Dim hBrush As PaintBrush
  Dim hImage As Image
  Dim fRadius As Float
  Dim Width As Integer = iX2 - iX1
  Dim Height As Integer = iY2 - iY1
  Dim aColor As Integer[]
  
  Select Case Me.Type
    
    Case TYPE_COLOR
      hBrush = Paint.Color(Color)
    
    Case TYPE_IMAGE
      Try hImage = Image.Load(Project.Dir &/ Path)
      If hImage Then
        hBrush = Paint.Image(hImage, iX1, iY1)
      Else
        hBrush = Paint.Color(&H808080&)
      Endif
    
    Case TYPE_LINEAR_GRADIENT
      aColor = Colors
      'If bIgnoreOpacity Then aColor = RemoveOpacity(aColor)
      hBrush = Paint.LinearGradient(Width * X1, Height * Y1, Width * X2, Height * Y2, aColor, Positions)
    
    Case TYPE_RADIAL_GRADIENT
      fRadius = Width * Radius
      aColor = Colors
      'If bIgnoreOpacity Then aColor = RemoveOpacity(aColor)
      hBrush = Paint.RadialGradient(Width * X1, Height * Y1, fRadius, Width * X2, Height * Y2, aColor, Positions)
  
  End Select
  
  Return hBrush
  
End

Public Sub _compare(hBrush As CReportBrush) As Integer
  
  If hBrush.ToString() = ToString() Then 
    Return 0
  Else
    Return Comp(Object.Address(Me), Object.Address(hBrush))
  Endif
  
End


Static Public Sub _get(sValue As String) As CReportBrush
  
  Dim hBrush As New CReportBrush
  Dim iPos As Integer
  Dim sType As String
  Dim ars As String[]
  
  sValue = Trim(sValue)
  
  iPos = InStr(sValue, "(")
  
  If iPos Then 
    If Right(sValue) <> ")" Then Error.Raise("Bad brush")
    sType = Left(sValue, iPos - 1)
    sValue = Mid$(sValue, iPos + 1, -1)
    ars = Split(sValue, ",", "[]")
  Else
    If Left(sValue) <> "#" Then Error.Raise("Bad brush")
    ars = [sValue]
  Endif
  
  Select Case LCase(sType)
    
    Case "image"
      
      hBrush.Type = TYPE_IMAGE
      'hBrush.Pattern = Image.Load(ars[0])
      hBrush.Path = UnQuote(ars[0])
      
    Case "radialgradient"
      
      hBrush.Type = TYPE_RADIAL_GRADIENT
      hBrush.X1 = CFloat(ars[0])
      hBrush.Y1 = CFloat(ars[1])
      hBrush.Radius = CFloat(ars[2])
      hBrush.X2 = CFloat(ars[3])
      hBrush.Y2 = CFloat(ars[4])
      hBrush.Colors = GetColorArray(ars[5])
      hBrush.Positions = GetFloatArray(ars[6])
      
    Case "lineargradient"
      
      hBrush.Type = TYPE_LINEAR_GRADIENT
      hBrush.X1 = CFloat(ars[0])
      hBrush.Y1 = CFloat(ars[1])
      hBrush.X2 = CFloat(ars[2])
      hBrush.Y2 = CFloat(ars[3])
      hBrush.Colors = GetColorArray(ars[4])
      hBrush.Positions = GetFloatArray(ars[5])
      
    Case Else
      
      hBrush.Color = StringToColor(ars[0])
      hBrush.Colors[0] = hBrush.Color
      
  End Select
  
Finally
  
  If hBrush.Colors.Count = 0 Then hBrush.Colors = [0, &hFFFFFF&]
  If hBrush.Colors.Count < 2 Then hBrush.Colors.Add(&hFFFFFF&)
  
  If hBrush.Positions.Count < 2 Then hBrush.Positions = [0.0, 1.0]
  
  Return hBrush
  
Catch
  
  Error.Raise("Bad brush")
  
End

Static Private Function GetColorArray(sValue As String) As Integer[]
  
  Dim ari As New Integer[]
  Dim s As String
  
  For Each s In Split(sValue)
    ari.Add(StringToColor(s))
  Next
  
  Return ari
  
End

Static Private Function GetFloatArray(sValue As String) As Float[]
  
  Dim arf As New Float[]
  Dim s As String
  
  For Each s In Split(sValue)
    arf.Add(CFloat(s))
  Next
  
  Return arf
  
End

Static Public Sub ColorToString(iColor As Integer) As String
  
  If iColor And &HFF000000 Then
    Return "#" & Hex$(iColor, 8)
  Else
    Return "#" & Hex$(iColor, 6)
  Endif
  
End

Static Public Sub StringToColor(sColor As String) As Integer
  
  Return Val("&H" & Mid$(sColor, 2) & "&")
  
End



Public Function ToString() As String
  
  Dim i As Integer
  Dim f As Float
  Dim sValue As String
  
  Select Case Type
    
    Case TYPE_COLOR
      
      sValue = ColorToString({Color})
      
    Case TYPE_LINEAR_GRADIENT, TYPE_RADIAL_GRADIENT
      
      If Type = TYPE_RADIAL_GRADIENT Then
        sValue = "RadialGradient(" & X1 & "," & Y1 & "," & Radius & "," & X2 & "," & Y2 & ",["
      Else
        sValue = "LinearGradient(" & X1 & "," & Y1 & "," & X2 & "," & Y2 & ",["
      Endif
      
      For Each i In Colors
        sValue &= ColorToString(i) & ","
      Next
      
      sValue = Left(sValue, -1)
      
      sValue &= "],["
      
      For Each f In Positions
        sValue &= f & ","
        
      Next
      sValue = Left(sValue, -1)
      
      sValue &= "])"
    
    Case TYPE_IMAGE
      
      sValue = "Image(" & Quote(Path) & ")"
      
  End Select
  
  Return svalue
  
End
