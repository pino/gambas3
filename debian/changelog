gambas3 (3.19.5-1) unstable; urgency=medium

  * Team upload
  * Drop armel support
  * Disable gtk3webview unavailable on hurd
  * New upstream version 3.19.5

  [ Pino Toscano ]
  * Enable webkit in gb.qt5 also on non-Linux architectures

 -- Bastian Germann <bage@debian.org>  Fri, 08 Nov 2024 10:29:20 +0000

gambas3 (3.19.3-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.19.3
  * gbx_number: align type with builtin check signature (Closes: #1077196)
  * gbx_exec_loop: align type with builtin check signature

 -- Bastian Germann <bage@debian.org>  Sat, 03 Aug 2024 12:59:01 +0200

gambas3 (3.19.1-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.19.1
  * Remove library B-Ds that worked around #960707 (Closes: #1067473)
  * Remove kfreebsd leftovers
  * Scan git for new versions

 -- Bastian Germann <bage@debian.org>  Fri, 22 Mar 2024 17:22:33 +0000

gambas3 (3.19.0-2) unstable; urgency=medium

  * Fix lintian warning about pkg-config
  * Correct lintian overrides

 -- Bastian Germann <bage@debian.org>  Fri, 16 Feb 2024 19:44:32 +0000

gambas3 (3.19.0-1) unstable; urgency=medium

  * New upstream version 3.19.0
  * d/copyright: Add new upstream info
  * Add new gambas3-gb-highlight

  [ Amin Bandali ]
  * patch: make 'gb.pdf' compile with poppler 24.02.0 (Closes: #1063926)

 -- Bastian Germann <bage@debian.org>  Thu, 15 Feb 2024 19:19:50 +0000

gambas3 (3.18.4-3) unstable; urgency=medium

  * Team upload

  [ Jeremy Bícha ]
  * Build with webkit2gtk 4.1 instead of 4.0

 -- Bastian Germann <bage@debian.org>  Thu, 04 Jan 2024 21:56:26 +0100

gambas3 (3.18.4-2) unstable; urgency=medium

  * Team upload
  * Revert "Drop clang (armel builds with gcc again)"
  * Fix lintian: debian-rules-uses-wrong-environment-variable

 -- Bastian Germann <bage@debian.org>  Sun, 05 Nov 2023 14:05:42 +0100

gambas3 (3.18.4-1) unstable; urgency=medium

  * Team upload
  * Drop clang (armel builds with gcc again)
  * Drop Bastian from Uploaders
  * New upstream version 3.18.4

 -- Bastian Germann <bage@debian.org>  Sat, 04 Nov 2023 23:29:27 +0100

gambas3 (3.18.3-1) unstable; urgency=medium

  * New upstream version 3.18.3

  [ Amin Bandali ]
  * Add debian/patches/gb-pdf-poppler-upper-bound.patch to remove the
    upper bound on poppler version for gb.pdf, so it could be built
    with poppler 23.08.0 and later as well.

 -- Bastian Germann <bage@debian.org>  Fri, 18 Aug 2023 22:13:04 +0200

gambas3 (3.18.2-3) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Don't use qtwebengine5-dev on mipsel; to be removed (Closes: #1041909)

 -- Bastian Germann <bage@debian.org>  Tue, 25 Jul 2023 10:07:47 +0200

gambas3 (3.18.2-2) unstable; urgency=medium

  * Disable SDL 1.2 components (Closes: #1038356)
  * Include gb.geom.component with wildcard

 -- Bastian Germann <bage@debian.org>  Mon, 19 Jun 2023 16:32:11 +0200

gambas3 (3.18.2-1) unstable; urgency=medium

  * New upstream version 3.18.2
  * Drop transitional packages (Closes: #1032701, #1032702, #1032703, #1032720)
  * Let -webkit packages depend on both -x11 and -wayland
  * Drop upstream patch

 -- Bastian Germann <bage@debian.org>  Wed, 14 Jun 2023 18:14:24 +0200

gambas3 (3.18.0-4) unstable; urgency=medium

  * Change dependencies to guarantee IDE startup
  * Remove Ian (disappeared) from Uploaders

 -- Bastian Germann <bage@debian.org>  Thu, 26 Jan 2023 09:11:56 +0100

gambas3 (3.18.0-3) unstable; urgency=high

  * Build armel with clang (gcc 12.2 fails)
  * Use the correct DEB_HOST_ARCH_OS var for cross compilation

 -- Bastian Germann <bage@debian.org>  Mon, 23 Jan 2023 16:32:17 +0100

gambas3 (3.18.0-2) unstable; urgency=medium

  * Add reserved font names

 -- Bastian Germann <bage@debian.org>  Tue, 17 Jan 2023 21:14:45 +0100

gambas3 (3.18.0-1) unstable; urgency=medium

  * New upstream version 3.18.0
  * gambas3-gb-gtk3-opengl: Install missing files
  * Add new components

 -- Bastian Germann <bage@debian.org>  Mon, 16 Jan 2023 18:32:36 +0100

gambas3 (3.17.3-2) unstable; urgency=medium

  * Patch: Import pcre2 variant when configured to use it (Closes: #1018019)

 -- Bastian Germann <bage@debian.org>  Wed, 24 Aug 2022 23:52:35 +0200

gambas3 (3.17.3-1) unstable; urgency=medium

  * Drop upstream patch

 -- Bastian Germann <bage@debian.org>  Sat, 23 Jul 2022 10:47:02 +0200

gambas3 (3.17.2-2) unstable; urgency=medium

  [ Nathan Pratta Teodosio ]
  * debian/patches/fix-build-poppler-22-06.patch (LP: #1980522)

 -- Bastian Germann <bage@debian.org>  Thu, 07 Jul 2022 17:00:00 +0200

gambas3 (3.17.2-1) unstable; urgency=medium

  * New upstream version 3.17.2

 -- Bastian Germann <bage@debian.org>  Sun, 10 Apr 2022 16:59:06 +0200

gambas3 (3.17.1-1) unstable; urgency=medium

  * New upstream version 3.17.1 (Closes: #1008758)
  * d/copyright: Add new license

 -- Bastian Germann <bage@debian.org>  Tue, 05 Apr 2022 11:44:31 +0200

gambas3 (3.17.0-1) experimental; urgency=medium

  * Drop upstream patch
  * New upstream version 3.17.0
  * Update copyright info for new upstream version
  * Remove no longer needed postgresql-server dep
  * Add new components
  * Depend on newer PCRE lib (Closes: #1000122)

 -- Bastian Germann <bage@debian.org>  Sat, 12 Mar 2022 22:34:19 +0100

gambas3 (3.16.3-3) unstable; urgency=medium

  * Remove postgresql-server-dev-all MultiArch hint

  [ Jeremy Bicha ]
  * Cherry-pick patch to build PDF support with C++17

 -- Bastian Germann <bage@debian.org>  Mon, 07 Feb 2022 23:14:47 +0100

gambas3 (3.16.3-2) unstable; urgency=medium

  [ Bastian Germann ]
  * Correct zstd component's description
  * d/changelog: Correct email in old entry
  * Set bage's debian.org email
  * Webview components need gui impl (LP: #1944540)

  [ Pino Toscano ]
  * Limit the Wayland component & packages to Linux only

 -- Bastian Germann <bastiangermann@fishpost.de>  Thu, 07 Oct 2021 02:16:48 +0200

gambas3 (3.16.3-1) unstable; urgency=medium

  * New upstream version 3.16.3

 -- Bastian Germann <bastiangermann@fishpost.de>  Sat, 11 Sep 2021 14:12:24 +0200

gambas3 (3.16.2-3) unstable; urgency=medium

  * Build qt5webview on mips64el

 -- Bastian Germann <bastiangermann@fishpost.de>  Sat, 28 Aug 2021 12:01:07 +0200

gambas3 (3.16.2-2) unstable; urgency=medium

  * Don't build qt5-webview where webview is not available.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 25 Aug 2021 11:01:19 +0200

gambas3 (3.16.2-1) unstable; urgency=medium

  * Upstream removed license violating file, no need for repacking
  * New upstream version 3.16.2

 -- Bastian Germann <bastiangermann@fishpost.de>  Mon, 23 Aug 2021 10:29:04 +0200

gambas3 (3.16.0+ds-1) experimental; urgency=medium

  * Mark :native for an arch-all pkg
  * Update patch
  * Add new components
  * Do not install files that no longer exist
  * Install new files for existing components
  * Depend on libcrypt-dev explicitly
  * Allow other libcurl implementations
  * Exclude sourceless file with license violation
  * New upstream version 3.16.0+ds

 -- Bastian Germann <bastiangermann@fishpost.de>  Thu, 18 Feb 2021 23:06:50 +0100

gambas3 (3.15.2-1) unstable; urgency=medium

  * New upstream version 3.15.2
  * Drop upstream patch
  * Clean translation files

 -- Bastian Germann <bastiangermann@fishpost.de>  Fri, 18 Sep 2020 20:16:53 +0200

gambas3 (3.15.1-3) unstable; urgency=medium

  * Correct case sensitivity in lintian override
  * Exclude gambas3's transitive dependencies
  * Disable more modules for non-Linux platforms
  * Enable JIT module for all archs
  * lintian: wildcard-matches-nothing-in-dep5-copyright
  * lintian: duplicate-in-relation-field

 -- Bastian Germann <bastiangermann@fishpost.de>  Tue, 25 Aug 2020 19:08:35 +0200

gambas3 (3.15.1-2) unstable; urgency=medium

  * Patch: Correct poppler version detection

 -- Bastian Germann <bastiangermann@fishpost.de>  Wed, 19 Aug 2020 09:41:08 +0200

gambas3 (3.15.1-1) unstable; urgency=medium

  * Let devel depend on runtime for gbh3
  * Remove postgres patch
  * New upstream version 3.15.1

 -- Bastian Germann <bastiangermann@fishpost.de>  Sat, 01 Aug 2020 16:16:29 +0200

gambas3 (3.15.0-3) unstable; urgency=medium

  * Add gbh3 to gambas3-devel
  * Add gb.test component to gambas3-runtime

 -- Bastian Germann <bastiangermann@fishpost.de>  Sun, 19 Jul 2020 13:21:24 +0200

gambas3 (3.15.0-2) unstable; urgency=medium

  * Source-only reupload

 -- Bastian Germann <bastiangermann@fishpost.de>  Mon, 13 Jul 2020 13:11:22 +0200

gambas3 (3.15.0-1) unstable; urgency=medium

  [ Bastian Germann ]
  * Make patches follow DEP-3
  * Fail on bad configuration
  * New upstream version 3.15.0
  * d/copyright: Add db.postgresql and web.gui
  * Remove the upstream man pages
  * Add new gb.poppler component
  * Add new gb.web.gui component
  * Let gambas3-gb-jit recommend gcc
  * Rename gambas3-script pkg to gambas3-scripter
  * Rename gambas3-dev pkg to gambas3-devel

  [ Gianfranco Costamagna ]
  * Upload to sid

 -- Bastian Germann <bastiangermann@fishpost.de>  Tue, 07 Jul 2020 03:45:04 +0200

gambas3 (3.14.3-4) unstable; urgency=medium

  * Patch: nonverbose build
  * Move gb.desktop files to the right pkg
  * Move templates to gambas3-ide
  * Remove gbh3 from gambas3-runtime

 -- Bastian Germann <bastiangermann@fishpost.de>  Sat, 20 Jun 2020 13:50:13 +0200

gambas3 (3.14.3-3) unstable; urgency=medium

  * Add Debian CI pipelines
  * Update Standards-Version; no changes required
  * gambas3-gb-web-form is deprecated
  * Drop gambas3-gb-gtk and gambas3-gb-gtk-opengl (Closes: #947655)
  * Default dh_install
  * Remove David Paleino from Uploaders
  * Let gambas3-ide depend on gambas3-gb-form-print (LP: #1876450)
  * Move gb.desktop.gambas to the right package (LP: #1877042)
  * Fix watch file according to new GitLab URL

 -- Bastian Germann <bastiangermann@fishpost.de>  Sun, 10 May 2020 16:55:48 +0200

gambas3 (3.14.3-2) unstable; urgency=medium

  * Link with libcurl's GnuTLS version

 -- Bastian Germann <bastiangermann@fishpost.de>  Sun, 23 Feb 2020 20:37:59 +0100

gambas3 (3.14.3-1) unstable; urgency=medium

  * New upstream version 3.14.3

 -- Bastian Germann <bastiangermann@fishpost.de>  Sat, 18 Jan 2020 23:47:39 +0100

gambas3 (3.14.2-2) unstable; urgency=medium

  * Source-only reupload (Closes: #948783)

 -- Bastian Germann <bastiangermann@fishpost.de>  Mon, 13 Jan 2020 23:10:27 +0100

gambas3 (3.14.2-1) unstable; urgency=medium

  * Set Standards-Version to 4.4.1
  * Add Uploader: Bastian Germann
  * New upstream version 3.14.2
  * gb.openssl: GPL-2+ with OpenSSL exception
  * Remove patches
  * Add new package gambas3-gb-form-print
  * Depend on non-transitional libncurses
  * Add patch to exclude pq_fprintf def
  * Add lintian overrides
  * Change gambas3-runtime's section
  * Rules-Requires-Root: no (lintian fix)

 -- Bastian Germann <bastiangermann@fishpost.de>  Sun, 22 Dec 2019 10:30:40 +0100

gambas3 (3.13.0-1) unstable; urgency=medium

  [ Bastian Germann ]
  * Add all hardening flags

  [ Gianfranco Costamagna ]
  * Team upload
  * Upload to unstable

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 02 Jul 2019 09:45:20 +0200

gambas3 (3.13.0-1~exp5) experimental; urgency=medium

  [ Bastian Germann ]
  * Bump debhelper-compat to 12

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 08 May 2019 16:16:12 +0200

gambas3 (3.13.0-1~exp4) experimental; urgency=medium

  * debian/patches/cae967dedb9aa5c1d4a317070d7f40e8c999513d.patch:
  * debian/patches/0f044b9512d29916bcd83e89fb07de6fa5b9ea61.patch:
  * debian/patches/d721a82377075ae3b6735f404435a2ef68ce2beb.patch:
    - upstream poppler build fixes
  * Tweak ppc64el termios build fix with upstream patch

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 08 May 2019 15:57:07 +0200

gambas3 (3.13.0-1~exp3) experimental; urgency=medium

  * Fix ppc64* build failure with patch inspired from similar issue:
    on bug: 810907 affecting src:repsnapper

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 06 May 2019 19:15:11 +0200

gambas3 (3.13.0-1~exp2) experimental; urgency=medium

  * Upload to Ubuntu without poppler patch

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 06 May 2019 18:08:00 +0200

gambas3 (3.13.0-1~exp1) experimental; urgency=medium

  [ Bastian Germann ]
  * New upstream version 3.13.0 (Closes: #922401, Closes: #923495)
  * Install the AppStream in non-legacy path
  * Remove all patches
  * Document CC0 licensed file

  [ Gianfranco Costamagna ]
  * Team upload to experimental, due to freeze
  * Cherry-pick and apply Ubuntu poppler patch, from Sebastien Bacher
    <seb128@ubuntu.com>

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 06 May 2019 17:56:26 +0200

gambas3 (3.12.2-1) unstable; urgency=medium

  * New upstream version 3.12.2

 -- Bastian Germann <bastiangermann@fishpost.de>  Tue, 15 Jan 2019 22:40:44 +0100

gambas3 (3.12.1-3) unstable; urgency=medium

  [ Bastian Germann ]
  * Let gambas3-gb-jit depend on gambas3-runtime
  * Let gambas3-ide depend on gambas3-gb-jit (Closes: #919212)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 14 Jan 2019 13:06:25 +0100

gambas3 (3.12.1-2) unstable; urgency=medium

  * Add jit dependency to -ide package (Closes: #919212)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 14 Jan 2019 12:57:52 +0100

gambas3 (3.12.1-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 3.12.1
  * Drop prefer_qt5 patch, upstream

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 08 Jan 2019 09:02:03 +0100

gambas3 (3.12.0-1) unstable; urgency=medium

  [ Bastian Germann ]
  * Update Standards-Version. No changes needed.
  * New upstream version 3.12.0
  * Enable JIT
  * Remove upstream patches
  * Define gb.jit files

  [ Gianfranco Costamagna ]
  * Team Upload
  * bump copyright years

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 03 Jan 2019 10:23:13 +0100

gambas3 (3.11.4-6) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team upload

  [ Jeremy Bicha ]
  * Cherry-pick Make-gb.pdf-compile-with-poppler-0.71.patch
    - upstream fix for new poppler

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 27 Nov 2018 12:04:09 +0100

gambas3 (3.11.4-5) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team upload

  [ Bastian Germann ]
  * Take dh compiler flags into account
  * Add missing install files
  * Correct two copyright paths
  * Make the gambas3-gb-gui-* components transitional
  * Provide man pages for *.gambas programs
  * Update the man pages according to --help

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 30 Oct 2018 08:56:48 +0100

gambas3 (3.11.4-4) unstable; urgency=medium

  * Use upstream desktop file and icon
  * Use dh_auto_configure
  * Do not use deprecated --list-missing on dh_install
  * Enable parallel build
  * Do not use multi-arch libdir
  * Only package installed files
  * Install gbs3.gambas in /usr/bin
  * Remove the now-upstream shared-mime-info
  * Remove unnecessary .links and .dirs
  * Add copyright information

 -- Bastian Germann <bastiangermann@fishpost.de>  Thu, 25 Oct 2018 15:42:55 +0200

gambas3 (3.11.4-3) unstable; urgency=medium

  [ Bastian Germann ]
  * Fix MIPS build
  * Update standards version
  * Add missing gambas3-gb-qt5-opengl to gambas3 meta package
  * Remove the deprecated components from gambas3 meta package
  * Make gambas3-dev independent from gambas3-runtime

  [ Gianfranco Costamagna ]
  * Team upload

 -- Bastian Germann <bastiangermann@fishpost.de>  Mon, 15 Oct 2018 22:49:22 +0200

gambas3 (3.11.4-2) unstable; urgency=medium

  * Fix manpage spelling errors
  * Fix undefined TRUE/FALSE

 -- Bastian Germann <bastiangermann@fishpost.de>  Mon, 15 Oct 2018 12:40:43 +0200

gambas3 (3.11.4-1) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team upload
  * New upstream release
  * Drop old transitional gb-desktop-gnome package, thanks Holger for
    the report and fix (Closes: #874760)
  * Update sections with new patch (Closes: #880695)
  * Bring back examples package

  [ Pino Toscano ]
  * Remove useless qtwebkit dependency
    (Closes: #867306)

  [ Bastian Germann ]
  * New upstream version 3.11.4
  * Update the watch file location
  * Remove the deprecated gambas3-gb-desktop-gnome-keyring package
    (Closes: #867930)
  * Update the version control links to salsa
  * Bump std-version to 4.1.4, no changes required
  * Add "Multi-Arch: same" for two packages
  * Modify the MIME icon sizes
  * Remove outdated Portland project links (Closes: #896665)
  * Remove all patches
  * Drop qt4 packages (Closes: #874896)
  * Add d/gbp.conf
  * Explicitly disable non-packaged components
  * Introduce one old and one new qt4 patch
  * Add gambas3-gb-term package
  * Add gambas3-gb-dbus-trayicon package
  * Add gambas3-gb-media-form package
  * Remove gambas3-gb-gui-trayicon and fix gambas3-ide
  * Add gambas3-gb-web-{feed,form} packages
  * Correct d/control syntax
  * Merge two packages into gambas3-runtime
  * Define wget as a dependency for gambas3-ide
  * Fix a lot of dependencies
  * Address lintian errors including new patch
  * Remove unused shlibs:Depends substitution variables
  * Transition to gmime 3.0 (Closes: #867348)
  * Set a new Maintainer team address (Closes: #899515)

 -- Bastian Germann <bastiangermann@fishpost.de>  Thu, 11 Oct 2018 17:18:31 +0200

gambas3 (3.9.2-2) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Drop sqlite2 old package
  * Fix gcc7 build failure with upstream patch (Closes: #853411)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 05 Jul 2017 19:25:54 +0200

gambas3 (3.9.2-1) unstable; urgency=medium

  * Team Upload.
  * New upstream release.
  * Bump std-version to 4.0.0
  * Bump compat level to 10

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 20 Jun 2017 19:05:16 +0200

gambas3 (3.9.1-3) unstable; urgency=medium

  * Team Upload
  * Drop libmodplug-dev, useless
    (Closes: #850525)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 07 Jan 2017 18:04:32 +0100

gambas3 (3.9.1-2) unstable; urgency=medium

  * Team Upload

  [ Josh Triplett ]
  * Add new programming language sections (Closes: #847533)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 09 Dec 2016 08:25:00 +0100

gambas3 (3.9.1-1) unstable; urgency=medium

  [ Ian Haywood ]
  * New upstream version (Closes: #837541, #838309)

  [ José L. Redrejo Rodríguez ]
  * Fixed build dependencies
  * Removed gitignore files from upstream when building the debs

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Sat, 08 Oct 2016 19:42:37 +0200

gambas3 (3.8.4-6) unstable; urgency=medium

  * debian/patches/openssl-fixes.patch:
    - fix openssl 1.1 incompatibilities (Closes: #828307)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 01 Jul 2016 08:36:09 +0200

gambas3 (3.8.4-5) unstable; urgency=medium

  * Team upload
  * Revert my previous change, because it breaks some arch:all
    build.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 30 Jun 2016 11:28:30 +0200

gambas3 (3.8.4-4) unstable; urgency=medium

  [ Pino Toscano ]
  * Fix compilation on hurd, by restricting the architecures list
    (Closes: #829056)
  * Remove useless kdelibs5-dev dependency (Closes: #829058)

  [ Gianfranco Costamagna ]
  * Team upload
  * Fix installation on hurd, by restricting the gambas3 dependencies
    (where not built)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 30 Jun 2016 10:11:41 +0200

gambas3 (3.8.4-3) unstable; urgency=medium

  [ Ian Haywood ]
  * patch to use Qt5 preferentially over Qt4. This way the IDE works when both Qt4 and Qt5 present.
  * Remove Conflicts on Qt4 for the the IDE as now unnecessary (Closes: #814862)

  [ Gianfranco Costamagna ]
  * Team upload.
  * Fix unsecure VCS uri.
  * Disable jit package, not llvm-3.6+ ready.
  * Bump std-version to 3.9.8, no changes required.
  * debian/patches/fix-build-failure.patch.
    - cherry-pick upstream r7740 to fix build failure.

  [ trek00@inbox.ru ]
  * Tweak gb.gui dependencies to not force many graphic implementations
    (Closes: #823908)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 15 Jun 2016 14:40:21 +0200

gambas3 (3.8.4-2) unstable; urgency=medium

  * Team upload.
  * Switch to libpng-dev to ease libpng transition (Closes: #809862).

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 05 Jan 2016 00:12:43 +0100

gambas3 (3.8.4-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable
  * Update libgsl0-dev to libgsl-dev, to ease gsl transition.
  * Remove deprecated menu file (Per the tech-ctte decision on #741573)
  * Disable qt4 webkit (Closes: #784465).
  * New upstream release, drop gnu_support patch, merged upstream
    (Closes: #807061).
  * Add breaks+replaces for gb-desktop{x11} package. (Closes: #787921)
  * Drop -examples package, dropped upstream.
  * Add new pacages: gambas3-gb-report2 gambas3-gb-sdl2-audio gambas3-gb-sdl2
    gambas3-gb-util gambas3-gb-util-web gambas3-gb-scanner
  * Add some new build-dependencies, needed for new sdl2 packages:
    libsdl2-dev, libsdl2-image-dev, libsdl2-mixer-dev, libsdl2-ttf-dev
  * Fix some install files.
  * Drop DejaVuSans.ttf sdl link, dropped in version 3.5.3
  * Add xdg-utils as build-dependency, needed to correctly handle mimes.
  * fix copyright file, adding missing svg CC-by-sa licenses, removing some old
    entries, making it really machine readable, add some public domain licenses
    (Closes: #787761)
  * Remove llvm-dev build-dependency, forcing llvm-3.5 (there is no planned
    porting on top of a newer llvm release
    (http://gambaswiki.org/bugtracker/edit?object=BUG.835).
  * Simplify libv4l-dev build-dependency.
  * Stop building gambas3-gb-inotify package on kfreebsd-* (missing libc header)
  * move webkit support in new qt5 package.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 16 Dec 2015 15:10:28 +0100

gambas3 (3.6.2-1) experimental; urgency=low

  * Team upload.

  [ José L. Redrejo Rodríguez ]
  * Fixes in debian/rules & debian control to fix lintian warnings and errors

  [ Gianfranco Costamagna ]
  * New upstream release. (Closes: #775452)
    - refresh patches.
    - drop patches merged upstream: detect_browser_debian, fix-llvm-3.5.patch
      fix_typos,
    - move kfreebsd_support into gnu_support, since kfreebsd is now upstream
      supported (cfr upstream issue 595 for the gnu support)
  * Rewrite rules file, dropping old hacks, and following new dh rules.
  * Bump standard version to 3.9.6, no changes required.
  * Wrap and sort debian directory.
  * Add appdata file
  * Add libgtk-3-dev as build dependency, needed for the new gtk3 package.
  * Add libdumb1, seems needed in the install target (spot by some build logs).
  * Drop PHP3 License, the code has been removed already in 3.5.4.
  * Drop SIL-1.1, file _default.bdf has been removed upstream.
  * Tweak debian/rules to remove the _default.bdf file.
  * New packages: gambas3-gb-desktop-x11, gambas3-gb-inotify,
    gambas3-gb-markdown, gambas3-gb-gtk3.
  * Fix some install files.

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Fri, 16 Jan 2015 17:24:39 +0100

gambas3 (3.5.4-2) unstable; urgency=medium

  * Team upload.

  [ Gianfranco Costamagna ]
  * Add fix-llvm-3.5.patch, fixing the llvm-3.5 build failure.
    (Closes: #763238)

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Mon, 13 Oct 2014 10:46:46 +0200

gambas3 (3.5.4-1) unstable; urgency=medium

  * Team upload.

  [ Gianfranco Costamagna ]
  * New upstream version. (Closes: #752625, #754209)
  * Added Vcs-* fields.
  * Add gambas3-gb-clipper as dependency of gambas3-ide
    (Closes: #749059), thanks to Shane Kerr.
  * Bump compat level to 9.
  * Remove d/p/find_postgresql_headers, addressed upstream.


  [ Pino Toscano ]
  * Adding parallel builds (Closes: #751217).

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Mon, 14 Apr 2014 10:46:46 +0200

gambas3 (3.5.2-2) unstable; urgency=low

  * New upstream version
  * New packages gb.media, gb.logging, gb.desktop.gnome
  * postgresql-server-dev-all needed to compile postgres
    drivers (Closes: #741789)

 -- Ian Haywood <ian@haywood.id.au>  Wed, 02 Apr 2014 16:23:48 +1100

gambas3 (3.5.2-1) unstable; urgency=low

  * New upstream version
  * Fix bug in gb.geom (Closes: #731487)
  * Added SIL font licence to debian/copyright
  * fix permissions on gb.sdl/data/src/_default.bdf

 -- Ian Haywood <ian@haywood.id.au>  Fri, 14 Feb 2014 14:41:15 +1100

gambas3 (3.5.1-1) unstable; urgency=low

  [ José L. Redrejo Rodríguez ]
  * New upstream release.
  * Remove previous Poppler patch, as upstream is updated. Thanks to Pino
    for its help while we were MIA.
  * Removed patch from Sebastian Ramacher as it's not needed with this
    new upstream version. Thanks to him and Dmitrijs Ledkovs for his NMU.
  * debian/control:
    - New gambas3 components.
    - Added new dependencies for the new Gambas3 components.
    - Fixed some build dependencies to match current Debian sid repo.
  * debian/patches/fix_typos (Closes: #690509, #690510)
  * debian/patches/kfreebsd_support (Closes: #710320) . Thanks to
    Steven Chamberlain.

  [ Ian  Haywood ]
  * Updated debian/copyright.
  * Fixed some grammar errors in the changelog.
  * Added gb.jit and its dependencies.
  * debian/rules: moving to dpkg-buildflags.

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Tue, 03 Sep 2013 12:52:14 +0200

gambas3 (3.1.1-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * Backport upstream r4730, r4731, and r4732 to fix compatibility with
    Poppler >= 0.20.x (Closes: #679885)

 -- Pino Toscano <pino@debian.org>  Tue, 23 Jul 2013 11:32:16 +0200

gambas3 (3.1.1-2.1) unstable; urgency=low

  [ Sebastian Ramacher ]
  * Non-maintainer upload.
  * Fix FTBFS with eglibc-2.7. (Closes: #701393)

  [ Dmitrijs Ledkovs ]
  * Non-maintainer upload

 -- Dmitrijs Ledkovs <xnox@debian.org>  Tue, 04 Jun 2013 23:16:32 +0100

gambas3 (3.1.1-2) unstable; urgency=low

  * debian/control:
   - Removed hardcoded dependency on libglew1.5 (Closes: #674080)
   - Replaced libpoppler-dev by libpoppler-private-dev build
     dependency (Closes: #673746)
   - Fine tuning some build dependencies trying to compile in freebsd archs

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Thu, 24 May 2012 13:00:18 +0200

gambas3 (3.1.1-1) unstable; urgency=low

  * First upload to Debian of gambas3 (Closes: #654424)

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Fri, 04 May 2012 18:21:24 +0200
